SELECT 'CREATE DATABASE hivemetastoredb'
WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = 'hivemetastoredb')\gexec
SELECT 'CREATE DATABASE oozie_metastore'
WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = 'oozie_metastore')\gexec
SELECT 'CREATE DATABASE teneo'
WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = 'teneo')\gexec
SELECT 'CREATE DATABASE hue'
WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = 'hue')\gexec
SELECT 'CREATE DATABASE dwh'
WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = 'dwh')\gexec
SELECT 'CREATE DATABASE airflow'
WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = 'airflow')\gexec

\c dwh;
create table if not exists public.interpretation_needed
(
    productmodelid integer not null,
    productmodel   varchar(255),
    language       char(64),
    description    varchar(99999)
);
