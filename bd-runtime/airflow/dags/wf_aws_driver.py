
from datetime import timedelta
from textwrap import dedent

from airflow import DAG
from airflow.operators.trigger_dagrun import TriggerDagRunOperator
from airflow.utils.dates import days_ago
from airflow.providers.apache.livy.operators.livy import LivyOperator

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
    # 'wait_for_downstream': False,
    # 'dag': dag,
    # 'sla': timedelta(hours=2),
    # 'execution_timeout': timedelta(seconds=300),
    # 'on_failure_callback': some_function,
    # 'on_success_callback': some_other_function,
    # 'on_retry_callback': another_function,
    # 'sla_miss_callback': yet_another_function,
    # 'trigger_rule': 'all_success'
}
with DAG(
        'wf_aws_driver',
        default_args=default_args,
        description='A simple test trigger another DAG',
        schedule_interval=None,
        start_date=days_ago(0),
        tags=['test'],
) as dag:
    dag.doc_md = dedent("""\
    Тестовый WF с вызовом другого WS
    """)

    aws_test2 = LivyOperator(
        task_id='aws_test2',
        dag=dag,
        livy_conn_id='livy_default',
        file='s3a://datagram/user/root/deployments/autogenerated_tr_aws_test_2/ru.neoflex.meta.etl2.spark.aws_test_2-1.0'
             '-SNAPSHOT.jar',
        proxy_user='root',
        args=[
            'HOME=/user',
            'USER=root',
            'WF_HOME=/user/root',
            'ROOT_WORKFLOW_ID=aws_test_2_729508210',
            'CURRENT_WORKFLOW_ID=aws_test_2_729508210',
            'SLIDE_SIZE=400',
            'FETCH_SIZE=1000',
            'PARTITION_NUM=1',
            'FAIL_THRESHOLD=1000',
            'DEBUG=true',
            'MASTER='
        ],
        num_executors=1,
        conf={
            'spark.shuffle.compress': 'false',
        },
        class_name='ru.neoflex.meta.etl2.spark.aws_test_2Job',
        polling_interval=5,
    )

    wf_aws_test = TriggerDagRunOperator(
        task_id='wf_aws_test',
        trigger_dag_id='wf_aws_test',
        wait_for_completion=True,
        poke_interval=5,
        dag=dag
    )
    aws_test2 >> wf_aws_test
