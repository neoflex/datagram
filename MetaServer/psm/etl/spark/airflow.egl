[%
  workflow.name.println("airflow.egl: ");
  var workflowId = Native("java.lang.System").identityHashCode(Sequence {});
  var JSONHelper = Native("java.lang.Thread").currentThread().getContextClassLoader().loadClass("ru.neoflex.meta.utils.JSONHelper");
  var pairs = Sequence  {};

%]#
# Workflow: [%=workflow.name%]
#
[%=workflow.declaration(workflowDeployment)%]
[%
@template
operation Workflow declaration(workflowDeployment) {
    var startDate = workflowDeployment.get("airflow").get("dagStartDate");
    var scheduleInterval = workflowDeployment.get("airflow").get("dagScheduleInterval");
%]
from airflow import DAG
from airflow.models import SkipMixin, BaseOperator, TaskInstance
from airflow.operators.bash import BashOperator
from airflow.operators.dummy import DummyOperator
from airflow.operators.python import PythonOperator
from airflow.operators.python import BranchPythonOperator
from airflow.operators.trigger_dagrun import TriggerDagRunOperator
[%if(isRunOnLivy()){%]
from airflow.providers.apache.livy.operators.livy import LivyOperator
[%}else{%]
from airflow.providers.apache.spark.operators.spark_submit import SparkSubmitOperator
[%}%]
from airflow.utils import timezone
from airflow.utils.dates import days_ago
from airflow.utils.trigger_rule import TriggerRule
from airflow.utils.state import State
from airflow.exceptions import AirflowFailException
from typing import Dict
from airflow.utils.session import create_session


class FinishOperator(BaseOperator, SkipMixin):

    def execute(self, context: Dict):
        excl_states = [State.SUCCESS, State.FAILED, State.SKIPPED, State.RUNNING]
        all_tasks = [t for t in context['dag_run'].get_task_instances() if t.state not in excl_states]
        if all_tasks:
            self.log.info('Skipping tasks: [ %s ]',
                          ', '.join([t.task_id + '(' + str(t.state) + ')' for t in all_tasks]))
            self.skip(context['dag_run'], context['ti'].execution_date, all_tasks)


class KillOperator(FinishOperator):

    def __init__(self, message, task_id: str, **kwargs) -> None:
        super().__init__(task_id=task_id, **kwargs)
        self.message = message

    def execute(self, context: Dict):
        super().execute(context)
        # self.log.info(self.message)
        raise AirflowFailException(self.message)


def on_failure_callback(context):
    ti = context['task_instance']
    task = ti.task
    downstream_tasks = task.downstream_list
    branch_rules = {TriggerRule.ONE_FAILED, TriggerRule.ALL_FAILED}
    branch_task_ids = {t.task_id for t in downstream_tasks if t.trigger_rule in branch_rules}
    for branch_task_id in list(branch_task_ids):
        branch_task_ids.update(task.dag.get_task(branch_task_id).get_flat_relative_ids(upstream=False))
    skip_tasks_ids = [t.task_id for t in downstream_tasks if t.task_id not in branch_task_ids]
    task.log.info("Skipping branches %s", skip_tasks_ids)
    if skip_tasks_ids:
        dag_run = ti.get_dagrun()
        now = timezone.utcnow()
        with create_session() as session:
            session.query(TaskInstance).filter(
                TaskInstance.dag_id == dag_run.dag_id,
                TaskInstance.execution_date == dag_run.execution_date,
                TaskInstance.task_id.in_(skip_tasks_ids),
                ).update(
                {
                    TaskInstance.state: State.SKIPPED,
                    TaskInstance.start_date: now,
                    TaskInstance.end_date: now,
                },
                synchronize_session=False,
            )
            session.commit()


default_args = {
}
with DAG(
        '[%=self.name%]',
        default_args=default_args,
        description="""[%=self.description%]""",
        default_view='graph',
        params={
        [%if (self.auditInfo <> null and self.auditInfo.get('changeDateTime') <> null) {%]
            'wf_change_dt': """[%=self.auditInfo.get('changeDateTime').toString()%]""",
        [%}%]
        },
        tags=[
        [%if (self.project <> null) {%]
            """[%=self.project.name%]""",
        [%}%]
        ],
        start_date=[%if (startDate.isDefined() and startDate.trim().length() > 0) {%]"""[%=startDate.toString()%]"""[%}else{%]days_ago(0)[%}%],
        schedule_interval=[%if (scheduleInterval.isDefined() and scheduleInterval.trim().length() > 0) {%]"""[%=scheduleInterval.toString()%]"""[%}else{%]None[%}%],
) as dag:
    [%for (node in self.nodes) {%]
    [%=node.declaration(workflowDeployment)%]
    [%}%]
    [%for (pair in pairs) {%]
    [%=pair[0]%] >> [%=pair[1]%]
    [%}%]
[%
}

@template
operation WFManualStart declaration(workflowDeployment) {
%]
[%=self.name%] = DummyOperator(
    task_id='[%=self.name%]'
)

[%
    pairs.add(Sequence {self.name, self.to.name});
}

@template
operation WFEnd declaration(workflowDeployment) {
%]
[%=self.name%] = FinishOperator(
    task_id='[%=self.name%]',
    trigger_rule=TriggerRule.ONE_SUCCESS,
)

[%
}

@template
operation WFKill declaration(workflowDeployment) {
%]
[%=self.name%] = KillOperator(
    task_id='[%=self.name%]',
    message='[%if (self.message.isDefined()) {%][%=self.message%][%} else {%]Workflow failed[%}%]',
    trigger_rule=TriggerRule.ONE_SUCCESS,
)

[%
}

@template
operation WFDecision declaration(workflowDeployment) {
%]
def _[%=self.name%](**context):
    ti = context['ti']
    def pull(task):
        return ti.xcom_pull(task_ids=task)
[%for (theCase in self.cases) {%]
    if [%=theCase.predicate%]: return '[%=theCase.to.name%]'
[%}%]
    return '[%=self.`default`.name%]'

[%=self.name%] = BranchPythonOperator(
    task_id='[%=self.name%]',
    python_callable=_[%=self.name%],
    on_failure_callback=on_failure_callback,
    trigger_rule=TriggerRule.ONE_SUCCESS,
)

[%
    for (theCase in self.cases) {
        pairs.add(Sequence {self.name, theCase.to.name});
    }
    pairs.add(Sequence {self.name, self.`default`.name});
}

@template
operation WFFork declaration(workflowDeployment) {
%]
[%=self.name%] = DummyOperator(
    task_id='[%=self.name%]',
    trigger_rule=TriggerRule.ONE_SUCCESS,
)

[%
    for (path in self.paths) {
        pairs.add(Sequence {self.name, path.name});
    }
}

@template
operation WFJoin declaration(workflowDeployment) {
%]
[%=self.name%] = DummyOperator(
    task_id='[%=self.name%]',
    trigger_rule=TriggerRule.ALL_SUCCESS,
)

[%
    pairs.add(Sequence {self.name, self.to.name});
}

@template
operation WFShell declaration(workflowDeployment) {
    var bash_command = self.exec;
    for (arg in self.args) {
      bash_command = bash_command + ' "' + arg + '"';
    }
%]
[%=self.name%] = BashOperator(
    task_id='[%=self.name%]',
    bash_command="""[%=bash_command%]""",
    [%if (self.captureOutput == true) {%]
    do_xcom_push=True,
    [%}%]
    on_failure_callback=on_failure_callback,
    trigger_rule=TriggerRule.ONE_SUCCESS,
)

_e_[%=self.name%] = DummyOperator(
    task_id='_e_[%=self.name%]',
    trigger_rule=TriggerRule.ONE_FAILED,
)

[%
    pairs.add(Sequence {self.name, self.ok.name});
    pairs.add(Sequence {self.name, '_e_' + self.name});
    pairs.add(Sequence {'_e_' + self.name, self.error.name});
}


@template
operation WFSubWorkflow declaration(workflowDeployment) {
%]
[%=self.name%] = TriggerDagRunOperator(
    task_id='[%=self.name%]',
    trigger_dag_id='[%=self.subWorkflow.name%]',
    wait_for_completion=[%if (self.noWait == true) {%]False[%} else {%]True[%}%],
    conf = {
    [%for (property in self.properties) {%]
        """[%=property.name%]""": """[%=property.value%]""",
    [%}%]
    },
    poke_interval=5,
    on_failure_callback=on_failure_callback,
    trigger_rule=TriggerRule.ONE_SUCCESS,
)

_e_[%=self.name%] = DummyOperator(
    task_id='_e_[%=self.name%]',
    trigger_rule=TriggerRule.ONE_FAILED,
)

[%
    pairs.add(Sequence {self.name, self.ok.name});
    pairs.add(Sequence {self.name, '_e_' + self.name});
    pairs.add(Sequence {'_e_' + self.name, self.error.name});
}

@template
operation WFTransformation declaration(workflowDeployment) {
    var fsUrl = "hdfs:";
    if (workflowDeployment.get("airflow").get("filesystem") <> null) {
      fsUrl = workflowDeployment.get("airflow").get("filesystem").get("url");
    }
%]
[%=self.name%] = [%if(isRunOnLivy()){%]LivyOperator([%}else{%]SparkSubmitOperator([%}%]
    task_id="""[%=self.name%]""",
    [%if(isRunOnLivy()){%]livy_conn_id[%}else{%]conn_id[%}%]="""[%=workflowDeployment.get("airflow").get("livyConnId")%]""",
    [%if(isRunOnLivy()){%]file[%}else{%]application[%}%]="""[%=fsUrl%][%=workflowDeployment.get("airflow").get("home")%]/[%=workflowDeployment.get("airflow").get("user")%]/deployments/[%=workflowDeployment.get("name")%]/lib/"""
         """ru.neoflex.meta.etl2.spark.[%=self.transformation.name%]-1.0-SNAPSHOT.jar""",
    [%if(isRunOnLivy()){%]proxy_user="""[%=workflowDeployment.get("airflow").get("user")%]""",[%}%]
    [%if(isRunOnLivy()){%]args[%}else{%]application_args[%}%]=[
        """HOME=[%=workflowDeployment.get("airflow").get("home")%]""",
        """USER=[%=workflowDeployment.get("airflow").get("user")%]""",
        """WF_HOME=[%=workflowDeployment.get("airflow").get("home")%]/[%=workflowDeployment.get("airflow").get("user")%]""",
        """ROOT_WORKFLOW_ID=[%=self.transformation.name%]_[%=workflowId%]""",
        """CURRENT_WORKFLOW_ID=[%=self.transformation.name%]_[%=workflowId%]""",
        """SLIDE_SIZE=[%=workflowDeployment.get("slideSize").ifUndefined(100)%]""",
        """FETCH_SIZE=[%=workflowDeployment.get("fetchSize").ifUndefined(1000)%]""",
        """PARTITION_NUM=[%=workflowDeployment.get("partitionNum").ifUndefined(3)%]""",
        """FAIL_THRESHOLD=[%=workflowDeployment.get("rejectSize").ifUndefined(1000)%]""",
        [%if (workflowDeployment.get("debug") == true) {%]
        """DEBUG=true""",
        [%}%]
        [%for (deployment in workflowDeployment.get("deployments")) {%]
        """JDBC_[%=deployment.get("softwareSystem").get("name")%]_URL=[%=JSONHelper.escape(deployment.get("connection").get("url"))%]""",
        """JDBC_[%=deployment.get("softwareSystem").get("name")%]_USER=[%=JSONHelper.escape(deployment.get("connection").get("user"))%]""",
        """JDBC_[%=deployment.get("softwareSystem").get("name")%]_DRIVER=[%=JSONHelper.escape(deployment.get("connection").get("driver"))%]""",
        """JDBC_[%=deployment.get("softwareSystem").get("name")%]_PASSWORD=[%=JSONHelper.escape(getPassword(deployment.get("connection")))%]""",
        """JDBC_[%=deployment.get("softwareSystem").get("name")%]_SCHEMA=[%=JSONHelper.escape(deployment.get("connection").get("schema"))%]""",
        [%}%]
        [%for (parameter in workflowDeployment.get("parameters")) {%]
        """[%=parameter.get("name")%]=[%if (parameter.get("expression") == true) {%][%=parameter.get("value")%][%} else {%][%=JSONHelper.escape(parameter.get("value"))%][%}%]""",
        [%}%]
        [%for (parameter in self.parameters) {%]
        """[%=parameter.name%]=[%if (parameter.expression == true) {%][%=parameter.value%][%} else {%][%=JSONHelper.escape(parameter.value)%][%}%]""",
        [%}%]
        'MASTER=',
    ],
    [%if(self.numExecutors <> null and self.numExecutors > 0) {%]
    num_executors=[%=self.numExecutors%],
    [%} else if (workflowDeployment.get("numExecutors").isDefined() and workflowDeployment.get("numExecutors") <> 0) {%]
    num_executors=[%=workflowDeployment.get("numExecutors")%],
    [%}%]
    [%if(self.executorCores <> null and self.executorCores > 0) {%]
    executor_cores=[%=self.executorCores%],
    [%} else if (workflowDeployment.get("executorCores").isDefined() and workflowDeployment.get("executorCores") <> 0) {%]
    executor_cores=[%=workflowDeployment.get("executorCores")%],
    [%}%]
    [%if(self.driverMemory <> null and self.driverMemory > 0) {%]
    driver_memory=[%=self.driverMemory%],
    [%} else if (workflowDeployment.get("driverMemory").isDefined() and workflowDeployment.get("driverMemory") <> 0) {%]
    driver_memory=[%=workflowDeployment.get("driverMemory")%],
    [%}%]
    [%if(self.executorMemory <> null and self.executorMemory > 0) {%]
    executor_memory=[%=self.executorMemory%],
    [%} else if (workflowDeployment.get("executorMemory").isDefined() and workflowDeployment.get("executorMemory") <> 0) {%]
    executor_memory=[%=workflowDeployment.get("executorMemory")%],
    [%}%]
    [%if(workflowDeployment.get("airflow").get("queue").isDefined() and workflowDeployment.get("airflow").get("queue").trim().length() > 0) {%]
    queue='[%=workflowDeployment.get("airflow").get("queue")%]',
    [%}%]
    conf={
       [%for (opt in workflowDeployment.get("sparkOpts")) {%]
       '[%=opt.get("name")%]': '[%=opt.get("value")%]',
       [%}%]
       [%for (opt in self.sparkOpts) {%]
       '[%=opt.name%]': '[%=opt.value%]',
       [%}%]
    },
    [%if(isRunOnLivy()){%]class_name[%}else{%]java_class[%}%]='ru.neoflex.meta.etl2.spark.[%=self.transformation.name%]Job',
    [%if(isRunOnLivy()){%]polling_interval=5,[%}%]
    on_failure_callback=on_failure_callback,
    trigger_rule=TriggerRule.ONE_SUCCESS,
)

_e_[%=self.name%] = DummyOperator(
    task_id='_e_[%=self.name%]',
    trigger_rule=TriggerRule.ONE_FAILED,
)

[%
    pairs.add(Sequence {self.name, self.ok.name});
    pairs.add(Sequence {self.name, '_e_' + self.name});
    pairs.add(Sequence {'_e_' + self.name, self.error.name});
}

@template
operation WFNode declaration(workflowDeployment) {
%]
[%=self.name%] = DummyOperator(
    task_id='[%=self.name%]',
    trigger_rule=TriggerRule.ONE_SUCCESS,
)

[%
}
operation isRunOnLivy(){
     return workflowDeployment.get("airflow").get("runOnLivy");
}

operation getPassword(entity) {
    var Common = Native("java.lang.Thread").currentThread().getContextClassLoader().loadClass("ru.neoflex.meta.utils.Common");
    var SymmetricCipher = Native("java.lang.Thread").currentThread().getContextClassLoader().loadClass("ru.neoflex.meta.utils.SymmetricCipher");
	var password = entity.get("password");
    if (not password.isDefined() or password == "") {
        password = Common.getPassword("rt.JdbcConnection." + entity.get("name") + ".password");
    }
    return SymmetricCipher.encrypt(password);
}

%]

