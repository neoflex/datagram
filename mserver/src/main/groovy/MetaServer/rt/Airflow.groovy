package MetaServer.rt

import MetaServer.utils.REST
import groovyx.net.http.ContentType
import groovyx.net.http.HttpResponseException
import groovyx.net.http.RESTClient
import org.apache.commons.lang3.StringUtils
import org.apache.commons.logging.Log
import org.apache.spark.sql.execution.datasources.jdbc.JDBCOptions
import ru.neoflex.meta.utils.Context
import ru.neoflex.meta.utils.SymmetricCipher
import org.apache.commons.logging.LogFactory
import ru.neoflex.meta.model.Database
import groovy.json.JsonSlurper
import groovy.json.JsonOutput

class Airflow {
    
    private final static Log logger = LogFactory.getLog(Airflow.class)
    
    public static getConnection(airflow, path) {
        URL url = new URL(airflow.http.replaceAll("/\\z", "") + path);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        
        //String encoded = Base64.getEncoder().encodeToString((atlas.userName + ":" + atlas.password).getBytes(StandardCharsets.UTF_8));  //Java 8
        //con.setRequestProperty("Authorization", "Basic " + encoded);
        //con.setRequestProperty("Content-Type", "application/json; charset=utf-8");
        con.setRequestProperty("X-Requested-By", InetAddress.getLocalHost().getHostAddress());
        return con;
    }
    
    public static Object readOutput(con) {
        BufferedReader input = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        
        while ((inputLine = input.readLine()) != null) {
            response.append(inputLine);
        }
        input.close();
        
        def jsonSlurper = new JsonSlurper()
        jsonSlurper.parseText(response.toString())
    }
    
    public static Object testConnection(Map entity, Map params = null) {
        def db = Database.new
        def airflow = db.get(entity)
        
        HttpURLConnection con = getConnection(airflow, '/health');
        con.setRequestMethod("GET");
        
        def rm = readOutput(con)
        logger.info(rm);
        return rm
    }
    
    public static Object getPools(Map entity, Map params = null) {
        def db = Database.new
        
        def jsonSlurper = new JsonSlurper()
        def object = jsonSlurper.parseText(params.get("entity"))
        
        def airflow = db.get(object)
        
        HttpURLConnection con = getConnection(airflow, '/pools');
        con.setRequestMethod("GET");
        
        def rm = readOutput(con)
        return rm
    }
    
    public static Object createPool(Map entity, Map params = null) {
        def db = Database.new
        
        def jsonSlurper = new JsonSlurper()
        def object = jsonSlurper.parseText(params.get("entity"))
        
        def airflow = db.get(object)
        
        HttpURLConnection con = getConnection(airflow, '/pools');
        con.setRequestMethod("POST");
        con.setDoOutput(true);
        
        def resp = []
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        def rb = JsonOutput.toJson(params.get("pool"))
        wr.writeBytes(rb);
        wr.flush();
        wr.close();
        def res = readOutput(con)
        resp.add(res)
        int responseCode = con.getResponseCode();
        return resp
    }
    
    public static Object deletePool(Map entity, Map params = null) {
        def db = Database.new
        
        def jsonSlurper = new JsonSlurper()
        def object = jsonSlurper.parseText(params.get("entity"))
        
        def airflow = db.get(object)
        
        HttpURLConnection con = getConnection(airflow, '/pools/' + params.get("pool").get("name"));
        con.setRequestMethod("DELETE");
        con.connect();
        
        con.setDoOutput(true);
        
        def resp = []
        def res = readOutput(con)
        resp.add(res)
        int responseCode = con.getResponseCode();
        return resp
    }
    
    public static Object editPool(Map entity, Map params = null) {
        deletePool(entity, params)
        createPool(entity, params)
        
    }
    public static findByProject(Object project) {
        if (project == null) return null
        else {
            def airflow =  Context.current.session.createQuery("from rt.Airflow where project.e_id = ${project?.e_id}").uniqueResult()
            return airflow
        }
    }

    public static getDefault() {
        def airflow = Context.current.session.createQuery("from rt.Airflow where isDefault = true").uniqueResult()
        return airflow
    }

    public static Map submitWorkflow(Map airflow, Map workflowDeployment, Map params = null) {
        def client = new RESTClient( airflow.http )
        if (StringUtils.isNotEmpty(airflow.apiUser) && StringUtils.isNotEmpty(airflow.apiPassword)) {
            client.auth.basic(airflow.apiUser, airflow.apiPassword)
        }
        def workflowName = workflowDeployment.start.name
        def i = 0, maxi = 10
        def is_paused = false
        while (i++ < maxi) {
            try {
                def respRead = client.get(
                        path : "/api/v1/dags/${workflowName}/details",
                        contentType : ContentType.JSON,
                        query : [:])
                def wf_change_dt = respRead.data?.params?.wf_change_dt
                def changeDateTime = workflowDeployment.start.auditInfo.changeDateTime.toString()
                if (changeDateTime != wf_change_dt) {
                    logger.info("Found OLD ${workflowName} remote:${wf_change_dt} local:${changeDateTime}")
                    if (i < maxi) {
                        logger.info("Waiting...")
                        sleep(3000)
                        continue;
                    }
                    return [result: false, problems: ["${workflowName} not found".toString()]]
                }
                logger.info("Dag: " + JsonOutput.prettyPrint(JsonOutput.toJson(respRead.data)))
                is_paused = respRead.data.is_paused
                break
            }
            catch (HttpResponseException e) {
                if (e.statusCode == 404) {
                    logger.info("${workflowName} not found")
                    if (i < maxi) {
                        logger.info("Waiting...")
                        sleep(3000)
                        continue;
                    }
                    return [result: false, problems: ["${workflowName} not found".toString()]]
                }
                throw e
            }
        }
        if (is_paused) {
            logger.info("Workflow ${workflowName} is paused. Unpause...")
            client.patch(
                    path : "/api/v1/dags/${workflowName}",
                    contentType : ContentType.JSON,
                    requestContentType: ContentType.JSON,
                    headers: ["X-Requested-By": InetAddress.getLocalHost().getHostAddress()],
                    body : [is_paused: false])
        }
        for (wf in WorkflowDeployment.getWorkflows(workflowDeployment)) {
            if (wf.name != workflowName) {
                def resp = client.get(
                        path : "/api/v1/dags/${wf.name}/details",
                        contentType : ContentType.JSON,
                        query : [:])?.data ?: [:]
                if (resp.is_paused) {
                    logger.info("Internal workflow ${wf.name} is paused. Unpause...")
                    client.patch(
                            path : "/api/v1/dags/${wf.name}",
                            contentType : ContentType.JSON,
                            requestContentType: ContentType.JSON,
                            headers: ["X-Requested-By": InetAddress.getLocalHost().getHostAddress()],
                            body : [is_paused: false])
                }
            }
        }
        def respRun = client.post(
                path : "/api/v1/dags/${workflowName}/dagRuns",
                contentType : ContentType.JSON,
                requestContentType: ContentType.JSON,
                headers: ["X-Requested-By": InetAddress.getLocalHost().getHostAddress()],
                body : [conf: params ?: [:]])
        logger.info("DagRun: " + JsonOutput.prettyPrint(JsonOutput.toJson(respRun.data)))

        return [result: true, problems: [], data:[DagRun: respRun.data]]
    }
}
