package MetaServer.rt

import com.google.gson.Gson
import groovy.io.FileType
import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.FSDataInputStream
import org.apache.hadoop.fs.FSDataOutputStream
import org.apache.hadoop.security.Credentials
import org.apache.hadoop.security.UserGroupInformation
import org.apache.hadoop.security.token.Token
import org.apache.hadoop.security.token.TokenIdentifier
import org.apache.hadoop.util.Shell
import org.springframework.util.AntPathMatcher

import java.lang.reflect.Field
import java.lang.reflect.Modifier
import java.nio.file.Path
import groovy.json.*

import java.security.PrivilegedAction
import java.time.LocalDateTime
import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE_TIME

class FileSystem {
    private final static Log logger = LogFactory.getLog(FileSystem.class);
    private org.apache.hadoop.fs.FileSystem fs;

    private FileSystem(org.apache.hadoop.fs.FileSystem fs){
        this.fs = fs;
    }

    private static boolean hacked = false
    private static void hack() {
        if (hacked) return
        hacked = true
        try {
            Field field = Shell.class.getDeclaredField("WINDOWS")
            field.setAccessible(true)
            Field modifiers = Field.class.getDeclaredField("modifiers")
            modifiers.setAccessible(true)
            modifiers.setInt(field, field.getModifiers() & ~Modifier.FINAL)
            field.set(null, false)
        }
        catch (Throwable e) {
            logger.warn(e.message)
        }
    }

    static FileSystem make(Map entity) {
        hack()
        def conf = new Configuration()
        def properties = entity.get("properties") as List<Map>
        properties.each {conf.set(it.get("name") as String, it.get("value") as String)}
        conf.set("dfs.datanode.use.datanode.hostname", "true")

        def url = entity.get("url") as String

        def proxyUser = UserGroupInformation.getCurrentUser()
        //proxyUser.addToken(new Token<TokenIdentifier>(user.getBytes(), pass.getBytes(), null, null))
        //proxyUser.doAs(new PrivilegedAction<FileSystem>() {
        //    @Override
        //    FileSystem run() {
                def system = org.apache.hadoop.fs.FileSystem.get(URI.create(url), conf)
                return new FileSystem(system)
        //    }
        //})

    }


    public void writeFileFromLocal(String uri, Path path) {
        FSDataOutputStream jobStream = fs.create(new org.apache.hadoop.fs.Path(URI.create(uri)))
        jobStream.write(java.nio.file.Files.readAllBytes(path));
        jobStream.hflush();
        jobStream.close();
    }

    public void writeDirectoryFromLocal(String path, File dir, List exclude) {
        def files = []
        def subDirs = []


        dir.eachFile (FileType.FILES) { file ->
            files << file
        }

        exclude.each{n->
            files.removeAll{ new AntPathMatcher().match(n as String, it.name as String)}
        }
        files.each {
            writeFileFromLocal("${path}/${it.name}".toString(), it.toPath())
        }

        dir.eachFile (FileType.DIRECTORIES) { directory ->
            subDirs << directory
        }
        subDirs.each {
            writeDirectoryFromLocal("${path}/${it.name}".toString(), it, exclude)
        }
    }

    public FSDataInputStream getFileToLocalStream(String uri) {
        FSDataInputStream stream = fs.open(new org.apache.hadoop.fs.Path(URI.create(uri)))
        return stream

    }


    public boolean exists(String path){
       fs.exists(new org.apache.hadoop.fs.Path(URI.create(path)))
    }

    public boolean delete(String path){
        fs.delete(new org.apache.hadoop.fs.Path(URI.create(path)), true);
    }


    public void mkdirs(String path) {
        fs.mkdirs(new org.apache.hadoop.fs.Path(URI.create(path)));
    }


    public boolean recreate(String path){
        boolean isExists = exists(path)
        if(isExists){
            delete(path)
            mkdirs(path)
        }
        isExists
    }




    public Map JsonView(String path){
        def res = [:]
        res["accessTime"] = 0
        res["blockSize"] = 0
        res["group"] = "owner"
        res["length"] = 0
        res["modificationTime"] = 0
        res["owner"] = "owner"
        res["pathSuffix"] = path
        res["permission"] = "rrwwxx"
        res["replication"] = 0
        res["type"] = fs.getFileStatus(new org.apache.hadoop.fs.Path(URI.create(path))).isDirectory()? "DIRECTORY" : "FILE"
        def children = [];
        def list = fs.listStatus(new org.apache.hadoop.fs.Path(URI.create(path)));
        list.each {
            def item = [:]
            item["accessTime"] = it.getAccessTime()
            item["blockSize"] = it.getBlockSize()
            item["group"] = it.getGroup()
            item["length"] = it.getLen()
            item["modificationTime"] = it.getModificationTime();
            item["owner"] = it.getOwner()
            item["pathSuffix"] = it.getPath().toUri();
            item["permission"] = it.getPermission().toString();
            item["replication"] = it.getReplication()
            item["type"] = it.isDirectory() ? "DIRECTORY" : "FILE";
            item["name"] = it.getPath().getName()
            children.add(item);
        }
        res['children'] = children
        return  res;

    }



}
